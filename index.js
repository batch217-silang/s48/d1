// console.log("hello");

let posts = [];
let count = 1;



/*[ADD POST]*/

// add data going to the storage posts = []
// querySelector = acces id or class
// submit event
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
// add element - push()
// {} json object

	// this line of code prevents our page from refreshing
	e.preventDefault();


	posts.push({
// the id is connected to count then use incrementation to run
// dom to access whatever is put then it will be pushed to posts = []
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value,
	});
	// all posts has id of one so we need to use incrementation ++ / count = count + 1, so everytime we add a data, it gets plus one, 2, 3, and so on ....
	count++;

	showPost(posts);
	alert ("Successfully added");
	// indicator that the data was added successfully
})



/*[SHOW POST]*/
const showPost = (post) => {
	let postEntries = "";
	// to get all the data from posts
	posts.forEach((post) => {
		// get all datas  from the empty array
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>`;
			// why? so that we want to enter tha data to the html
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}




/*[EDIT POST]*/
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-title-edit").value = title;
	document.querySelector("#txt-body-edit").value = body;
}




/*[UPDATE POST]*/
// we need for loop to compare whether a data is same as the id of data being called 
document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){

			posts[i].title = document.querySelector("#txt-title-edit").value;
			posts[i].body = document.querySelector("#txt-body-edit").value;

			showPost(posts);
			alert("Successfully Updated");

			break;
		}else{
			alert("Error Detected");
		}
	}

})




/*[DELETE POST]*/
const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id){
			return post;
		}
	})
	document.querySelector(`#post-${id}`).remove();
}